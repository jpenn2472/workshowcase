function checkObj(obj, checkProp) {
  // Only change code below this line
  if(obj.hasOwnProperty(checkProp) == true) {
    console.log(checkProp)
    return obj[checkProp];
   
  } else {
  return "Not Found";
  }
  
  // Only change code above this line
}
//Even though I'm still taking in the value directly
//I need to return it with the obj[checkProp] syntax because
//I need to return the property value. CheckProp is just the key
//in the pair attached to that specific property of the object