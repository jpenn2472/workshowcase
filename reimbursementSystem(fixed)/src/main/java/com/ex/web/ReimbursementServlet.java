package com.ex.web;

import com.ex.models.Reimbursements;
import com.ex.services.ReimbursementService;
import com.ex.web.exceptions.InitializationException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class ReimbursementServlet extends HttpServlet {
    private ReimbursementService reimbursementService;
    private final Logger LOG = Logger.getLogger(this.getClass());

    @Override
    public void init(ServletConfig config) throws ServletException {
        LOG.info("ReimbursementServlet initializing");
        ServletContext context = config.getServletContext();
        String serviceLookupName = config.getInitParameter("reimbursementServiceLookupName");
        reimbursementService = (ReimbursementService) context.getAttribute(serviceLookupName);

        if (reimbursementService == null) {
            LOG.warn("ReimbursementServlet Initialization failed");
            InitializationException ie = new InitializationException("Missing dependency " + reimbursementService.getClass().getName());
            throw new ServletException("Error initializing ReimbursementServlet", ie);
        }
        LOG.info("ReimbursementServlet initialization complete");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String pathInfo = req.getPathInfo();
        boolean queryComplete = false;
        System.out.println("It Is Working");

        LOG.debug("Request for " + pathInfo);
        List<Reimbursements> reimbursements = null;
        switch (pathInfo) {
            case "/":
                reimbursements = retrieveReimbursements();
                queryComplete = true;
                break;
        }


        if (queryComplete) {
            System.out.println("Query Complete");
            ObjectMapper objectMapper = new ObjectMapper();
            String ret = objectMapper.writeValueAsString(reimbursements);
            resp.getWriter().write(ret);
            resp.setStatus(200);
            resp.setContentType("application/json");
            resp.setHeader("Access-Control-Allow-Origin", "http://localhost:3000");
            resp.setHeader("Access-Control-Allow-Method", "*");
            resp.setHeader("ACCESS-CONTROL-ALLOW-HEADERS", "Content-type");

        } else {
            System.out.println("Query Not Complete");
            resp.setStatus(404);

        }
        return;
    }

    private List<Reimbursements> retrieveReimbursements() {
        return this.reimbursementService.getAllReimbursments();
    }
}
