package com.ex.web.listener;

import com.ex.models.Reimbursements;
import com.ex.persistence.*;
import com.ex.services.EmployeeService;
import com.ex.services.ManagerService;
import com.ex.services.ReimbursementService;
import com.ex.web.exceptions.InitializationException;
import org.apache.log4j.Logger;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

//Thought I needed two so I changed the name. Fix back later
public class ContextLoadListenerReim implements ServletContextListener {
    private final Logger LOG = Logger.getLogger(ContextLoadListenerReim.class);
    ConnectionFactory connectionFactory;

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ServletContext context = servletContextEvent.getServletContext();
        initialzeConnectionFactory(context);
        initalizeDataAccess(context);
        initalizeServices(context);
        initializeDataAccessemp(context);
        initalizeServicesemp(context);
//        initializeDataAccessman(context);
//        initalizeServicesman(context);
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        LOG.info("Cleaning up context");
        connectionFactory.destroy();
    }

    //will replace database connection with mine
    private void initialzeConnectionFactory(ServletContext context) {
        LOG.info("Initializing database connection factory");
        connectionFactory = new PostgresConnectionFactory(
                "jdbc:postgresql://database-3.cdqkj5hffk1q.us-east-2.rds.amazonaws.com:5432/postgres",
                "postgresjp", "P3nnfam1ly!");
        context.setAttribute("connectionFactory", connectionFactory);
        LOG.info("ConnectionFactory added to context");
    }

    private void initalizeDataAccess(ServletContext context) {
        LOG.info("Initializing data access");
        ReimbursementPersistenceImpl reimbursementPersistence = new ReimbursementPersistenceImpl(connectionFactory);
        context.setAttribute("reimbursementPersistence", reimbursementPersistence);
        LOG.info("Data access initialized");
    }

    private void initalizeServices(ServletContext context) {
        LOG.info("Initializing service layer");
        ReimbursementPersistenceImpl reimbursementPersistenceImpl = (ReimbursementPersistenceImpl) context.getAttribute("reimbursementPersistence");

        if (reimbursementPersistenceImpl == null) {
            throw new InitializationException("Application not initialize correctly, missing dependency " + reimbursementPersistenceImpl.getClass().getName());
        } else {
            ReimbursementService reimbursementService = new ReimbursementService(reimbursementPersistenceImpl);
            context.setAttribute("reimbursementService", reimbursementService);
        }
        LOG.info("Service layer initialized");
    }
//Man calls
    private void initializeDataAccessman(ServletContext context) {
        LOG.info("Initializing data access");
        ManagerPersistenceImpl managerPersistence = new ManagerPersistenceImpl(connectionFactory);
        context.setAttribute("managerPersistence", managerPersistence);
        LOG.info("Data access initialized");
    }
    private void initalizeServicesman(ServletContext context) {
        LOG.info("Initializing service layer");
        ManagerPersistenceImpl managerPersistence = (ManagerPersistenceImpl) context.getAttribute("managerPersistence");

        if(managerPersistence == null) {
            throw new InitializationException("Application not initialize correctly, missing dependency " + managerPersistence.getClass().getName());
        } else {
            ManagerService managerService = new ManagerService(managerPersistence);
            context.setAttribute("managerService", managerService);
        }
        LOG.info("Service layer initialized");
    }
    //Emp calls
    private void initializeDataAccessemp(ServletContext context) {
        LOG.info("Initializing data access");
        EmployeePersistenceImpl employeePersistence = new EmployeePersistenceImpl(connectionFactory);
        context.setAttribute("employeePersistence", employeePersistence);
        LOG.info("Data access initialized");
    }
    private void initalizeServicesemp(ServletContext context) {
        LOG.info("Initializing service layer");
        EmployeePersistenceImpl employeePersistence = (EmployeePersistenceImpl) context.getAttribute("employeePersistence");

        if(employeePersistence == null) {
            throw new InitializationException("Application not initialize correctly, missing dependency " + employeePersistence.getClass().getName());
        } else {
            EmployeeService employeeService = new EmployeeService(employeePersistence);
            context.setAttribute("employeeService", employeeService);
        }
        LOG.info("Service layer initialized");
    }
}
