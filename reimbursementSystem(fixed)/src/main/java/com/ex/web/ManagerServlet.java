package com.ex.web;

import com.ex.models.Employee;
import com.ex.models.Manager;
import com.ex.models.Reimbursements;
import com.ex.services.ManagerService;
import com.ex.services.ReimbursementService;
import com.ex.web.exceptions.InitializationException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ManagerServlet extends HttpServlet {
    private ManagerService managerService;
    private final Logger LOG = Logger.getLogger(this.getClass());

    @Override
    public void init(ServletConfig config) throws ServletException {
        LOG.info("ManagerServlet initializing");
        ServletContext context = config.getServletContext();
        String serviceLookupName = config.getInitParameter("managerServiceLookupName");
        managerService = (ManagerService) context.getAttribute(serviceLookupName);

        if (managerService == null) {
            LOG.warn("ManagerServlet Initialization failed");
            InitializationException ie = new InitializationException("Missing dependency " + managerService.getClass().getName());
            throw new ServletException("Error initializing ManagerServlet", ie);
        }
        LOG.info("ManagerServlet initialization complete");
    }
    //change to post
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String pathInfo = req.getPathInfo();
        boolean queryComplete = false;

        LOG.debug("Request for " + pathInfo);
        List<Manager> managers = null;
        switch (pathInfo) {
            //deleted second case may need later
            case "/":
                managers = retrieveManagers();
                queryComplete = true;
        break;
        }

        if (queryComplete) {
            ObjectMapper objectMapper = new ObjectMapper();
            String ret = objectMapper.writeValueAsString(managers);
            resp.getWriter().write(ret);
            resp.setStatus(200);
            resp.setContentType("application/json");
            resp.setHeader("Access-Control-Allow-Origin", "http://localhost:3000");
            resp.setHeader("Access-Control-Allow-Method", "*");

        } else {
            resp.setStatus(404);
        }

        return;
    }

    private List<Manager> retrieveManagers() {

        return this.managerService.getAllManagers();
    }
}