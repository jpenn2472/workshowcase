package com.ex.web;

import com.ex.models.Employee;
import com.ex.models.Reimbursements;
import com.ex.services.EmployeeService;
import com.ex.services.ReimbursementService;
import com.ex.web.exceptions.InitializationException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class EmployeeServlet extends HttpServlet {
    private EmployeeService employeeService;
    private final Logger LOG = Logger.getLogger(this.getClass());

    @Override
    public void init(ServletConfig config) throws ServletException {
        LOG.info("EmployeeServlet initializing");
        ServletContext context = config.getServletContext();
        String serviceLookupName = config.getInitParameter("employeeServiceLookupName");
        employeeService = (EmployeeService) context.getAttribute(serviceLookupName);

        if (employeeService == null) {
            LOG.warn("EmployeeServlet Initialization failed");
            InitializationException ie = new InitializationException("Missing dependency " + employeeService.getClass().getName());
            throw new ServletException("Error initializing EmployeeServlet", ie);
        }
        LOG.info("EmployeeServlet initialization complete");
    }
//change to post
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String pathInfo = req.getPathInfo();
        boolean queryComplete = false;

        LOG.debug("Request for " + pathInfo);
        List<Employee> employees = null;
        switch (pathInfo) {
            //deleted second case may need later
            case "/":
                employees = retrieveEmployees();
                queryComplete = true;
        }

        if (queryComplete) {
            ObjectMapper objectMapper = new ObjectMapper();
            String ret = objectMapper.writeValueAsString(employees);
            resp.getWriter().write(ret);
            resp.setStatus(200);
            resp.setContentType("application/json");
//            resp.setHeader("Access-Control-Allow-Origin", "http://localhost:3000");
//            resp.setHeader("Access-Control-Allow-Method", "*");
            //resp.setHeader("Access-Control-Allow-Origin", "http://localhost:3000");
        } else {
            resp.setStatus(404);
        }
        return;
    }

    private List<Employee> retrieveEmployees() {

        return this.employeeService.getAllEmployee();
    }
}
