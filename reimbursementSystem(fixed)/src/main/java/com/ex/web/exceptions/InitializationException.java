package com.ex.web.exceptions;

public class InitializationException extends RuntimeException {
    public InitializationException() {
    }

    public InitializationException(String msg) {
        super(msg);
    }

    public InitializationException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
