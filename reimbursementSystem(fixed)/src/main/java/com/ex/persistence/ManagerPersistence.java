package com.ex.persistence;

import com.ex.models.Employee;
import com.ex.models.Manager;

import java.util.List;

public interface ManagerPersistence extends Persistable<Manager, Integer> {

    Manager getById(Manager id);
    List<Manager> getAll();
    List<Manager> delete(Manager obj);
    void update(Manager obj);
}

