package com.ex.persistence;

import com.ex.persistence.exceptions.ConnectionException;

import java.sql.Connection;

public interface ConnectionFactory {
    Connection newConnection();
    void destroy();
}
