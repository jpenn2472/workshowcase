package com.ex.persistence;

import com.ex.models.Employee;

import java.util.List;



    public interface EmployeePersistence extends Persistable<Employee, Integer> {
        List<Employee> getByPublisher(String publisherName);
        List<Employee> getByPlatform(String platformName);
        List<Employee> delete();
        List<Employee> getAll();

    }


