package com.ex.persistence;

import com.ex.models.Reimbursements;

import java.util.List;

public interface ReimbursementPersistence extends Persistable<Reimbursements, Integer> {
    List<Reimbursements> getByPublisher(String publisherName);
    List<Reimbursements> getByPlatform(String platformName);
    List<Reimbursements> getByEsrbRating(String rating);
    List<Reimbursements> getAllReimbursements();

}
