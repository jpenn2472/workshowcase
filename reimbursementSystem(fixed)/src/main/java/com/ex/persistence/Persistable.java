package com.ex.persistence;

import java.util.List;
//Hindsight it finally made sense to why you would want me to use these instead of writing my own. Will incorporate correctly in the future.
public interface Persistable <T, ID> {
    T getById(ID id);
    List<T> getAll();
    ID save(T obj);
    List<T> delete(T obj);
    void update(T obj);
}
