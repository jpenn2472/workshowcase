package com.ex.persistence;

import com.ex.models.Manager;

import com.ex.models.Employee;
import com.ex.persistence.exceptions.ConnectionException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EmployeePersistenceImpl implements EmployeePersistence {
    private ConnectionFactory connectionFactory;

    public EmployeePersistenceImpl(ConnectionFactory connectionFactory){
        this.connectionFactory = connectionFactory;
    }

    @Override
    //change to delete query
    public List <Employee> delete() {

        List<Employee> employees = new ArrayList<>();
        String query = "select * from  Employee";

        try(Connection c = connectionFactory.newConnection();
            Statement statement = c.createStatement();
            ResultSet rs = statement.executeQuery(query)){
            employees = createEmployeeCollection(rs);

        } catch (SQLException e) {
            throw new ConnectionException("An error occurred while querying in " + this.getClass().getName() + "#getAll", e);
        }
        return employees;
    }
    public List<Employee> getAll() {
        List<Employee> employees = new ArrayList<>();
        String query = "select * from  Employee";

        try(Connection c = connectionFactory.newConnection();
            Statement statement = c.createStatement();
            ResultSet rs = statement.executeQuery(query)){
            employees = createEmployeeCollection(rs);

        } catch (SQLException e) {
            throw new ConnectionException("An error occurred while querying in " + this.getClass().getName() + "#getAll", e);
        }
        return employees;
    }


    private List<Employee> createEmployeeCollection(ResultSet rs) {
        List<Employee> employees = new ArrayList<>();
        try {
            while (rs.next()) {
                Employee emp;
                final double empId = rs.getDouble(1);
                emp = employees.stream().filter(g -> g.getEmpid() == (empId)).findFirst().orElse(null);
                if (!employees.isEmpty() && emp != null) {
                    // just get the next platform
                } else {

                    Employee empid = new Employee();

//                    Employee fname = new Employee();
//                    Employee lname = new Employee();
//                    Employee empEmail = new Employee();


                    empid.setEmpid(rs.getDouble("empid"));
                    empid.setFname(rs.getString("fname"));
                    empid.setLname(rs.getString("lname"));
                    empid.setemail(rs.getString("email"));


                    employees.add(empid);
                   ;


                }
            }

            return employees;
        } catch (SQLException ex) {
            throw new ConnectionException("Error creating games list", ex);
        }
    }

    @Override
    public Employee getById(Integer integer) {


        return null;
    }


    @Override
    public Integer save(Employee obj) {
        return null;
    }

    @Override
    public List<Employee> delete(Employee obj) {
        return null;
    }

    @Override
    public List<Employee> getByPublisher(String publisherName) {
        return null;
    }

    @Override
    public List<Employee> getByPlatform(String platformName) {
        return null;
    }





    @Override
    public void update(Employee obj) {

    }




    }