package com.ex.persistence;

import com.ex.models.Manager;

import com.ex.persistence.exceptions.ConnectionException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ManagerPersistenceImpl implements ManagerPersistence {
    private ConnectionFactory connectionFactory;

    public ManagerPersistenceImpl(ConnectionFactory connectionFactory){
        this.connectionFactory = connectionFactory;
    }


    //change to delete query
    public List <Manager> delete() {

        List<Manager> managers = new ArrayList<>();
        String query = "select * from  Manager";

        try(Connection c = connectionFactory.newConnection();
            Statement statement = c.createStatement();
            ResultSet rs = statement.executeQuery(query)){
            managers = createManagerCollection(rs);

        } catch (SQLException e) {
            throw new ConnectionException("An error occurred while querying in " + this.getClass().getName() + "#getAll", e);
        }
        return managers;
    }

    @Override
    public Manager getById(Manager id) {
        return null;
    }

    @Override
    public List<Manager> getAll() {
        return null;
    }

    public List<Manager> getAllManagers() {
        List<Manager> managers = new ArrayList<>();
        String query = "select * from  Manager";

        try(Connection c = connectionFactory.newConnection();
            Statement statement = c.createStatement();
            ResultSet rs = statement.executeQuery(query)){
            managers = createManagerCollection(rs);

        } catch (SQLException e) {
            throw new ConnectionException("An error occurred while querying in " + this.getClass().getName() + "#getAll", e);
        }
        return managers;
    }


    private List<Manager> createManagerCollection(ResultSet rs) {
        List<Manager> managers = new ArrayList<>();
        try {
            while (rs.next()) {
                Manager man;
                final double manid = rs.getDouble(1);
                man = managers.stream().filter(g -> g.getManid() == (manid)).findFirst().orElse(null);
                if (!managers.isEmpty() && man != null) {
                    // just get the next platform
                } else {


//                    private double manID;
//                    private String fname;
//                    private String lname;
//                    private String manEmail;
//                    private String deptName;
                    Manager mannid = new Manager();

                    Manager fname = new Manager();
                    Manager lname = new Manager();
                    Manager manEmail = new Manager();


                    mannid.setManid(rs.getDouble("manid"));
                    fname.setFname(rs.getString("fname"));
                    lname.setLname(rs.getString("lname"));
                    manEmail.setManEmail(rs.getString("manemail"));


                    managers.add(mannid);
                    managers.add(fname);
                    managers.add(lname);
                    managers.add(manEmail);


                }
            }

            return managers;
        } catch (SQLException ex) {
            throw new ConnectionException("Error creating games list", ex);
        }
    }
//Hindsight it finally made sense to why you would want me to use these instead of writing my own. Will incorporate correctly in the future.
    @Override
    public Manager getById(Integer integer) {


        return null;
    }


    @Override
    public Integer save(Manager obj) {
        return null;
    }

    @Override
    public List<Manager> delete(Manager obj) {
        return null;
    }





    @Override
    public void update(Manager obj) {

    }




}