package com.ex.persistence;

import com.ex.models.Manager;
import com.ex.models.Reimbursements;
import com.ex.models.Employee;
import com.ex.persistence.exceptions.ConnectionException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ReimbursementPersistenceImpl implements ReimbursementPersistence {
    private ConnectionFactory connectionFactory;

    public ReimbursementPersistenceImpl(ConnectionFactory connectionFactory){
        this.connectionFactory = connectionFactory;
    }

    @Override
    public Reimbursements getById(Integer integer) {
        return null;
    }

    @Override
    public List<Reimbursements> getAll() {
        return null;
    }


    public List<Reimbursements> getAllReimbursements() {
        List<Reimbursements> reimbursements = new ArrayList<>();
        String query = "select * from  reimbursements";

        try(Connection c = connectionFactory.newConnection();
            Statement statement = c.createStatement();
            ResultSet rs = statement.executeQuery(query)){
            reimbursements = createReimbursementsCollection(rs);

        } catch (SQLException e) {
            throw new ConnectionException("An error occurred while querying in " + this.getClass().getName() + "#getAll", e);
        }
        return reimbursements;
    }

    @Override
    public Integer save(Reimbursements obj) {
        return null;
    }

    @Override
    public List<Reimbursements> delete(Reimbursements obj) {
        return null;
    }


    @Override
    public void update(Reimbursements obj) {

    }
//change query and method name
    @Override
    public List<Reimbursements> getByPublisher(String publisherName) {

        List<Reimbursements> games = new ArrayList<>();
        String query = "select games.id, games.title, games.esrb_rating, publishers.name as pub_name, platforms.name as plat_name " +
                "from games inner join publishers on games.publisher = publishers.id " +
                " and publishers.name like ? " +
                "left join games_platforms on games.id = games_platforms.game_id " +
                "left join platforms on games_platforms.platforms_id = platforms.id " +
                "group by games.id, games.title, games.esrb_rating, publishers.name, platforms.name";

        try(Connection c = connectionFactory.newConnection();
            PreparedStatement statement = c.prepareStatement(query)){

            statement.setString(1, publisherName);
            ResultSet rs = statement.executeQuery();
            games = createReimbursementsCollection(rs);
            return games;

        } catch (SQLException e) {
            throw new ConnectionException("An error occurred while querying in " + this.getClass().getName() + "#getByPublisher", e);
        }
    }

    @Override
    public List<Reimbursements> getByPlatform(String platformName) {
        return null;
    }

    @Override
    public List<Reimbursements> getByEsrbRating(String rating) {
        return null;
    }

//    @Override
//    public Integer save(Reimbursements obj) {
//        return null;
//    }
//
//    @Override
//    public void delete(Reimbursements obj) {
//
//    }
//
//    @Override
//    public void update(Reimbursements obj) {
//
//    }

    private List<Reimbursements> createReimbursementsCollection(ResultSet rs) {
        List<Reimbursements> reimbursements = new ArrayList<>();
        try {
            while(rs.next()) {
                Reimbursements nextreimid;
                final double reimId = rs.getDouble(1);
                nextreimid = reimbursements.stream().filter(g -> g.getReimId() == (reimId)).findFirst().orElse(null);
                if (!reimbursements.isEmpty() && nextreimid != null) {
                    // just get the next platform
                } else {

                    nextreimid = new Reimbursements();
//                   Reimbursements nextempid = new Reimbursements();
//                    Reimbursements status = new Reimbursements();
//                    Reimbursements amount = new Reimbursements();
//                    Reimbursements manId = new Reimbursements();
//                    Reimbursements requDay = new Reimbursements();
//                    Reimbursements reason = new Reimbursements();


                    nextreimid.setReimId(rs.getDouble("reimid"));
                    nextreimid.setEmpId(rs.getDouble("empid"));
                    nextreimid.setStatus(rs.getString("status"));
                    nextreimid.setAmount(rs.getDouble("amount"));
                    nextreimid.setManId(rs.getDouble("manid"));
                    nextreimid.setRequDay(rs.getString("requestday"));
                    nextreimid.setReason(rs.getString("reason"));


                    reimbursements.add(nextreimid);
//                    reimbursements.add(nextempid);
//                    reimbursements.add(status);
//                    reimbursements.add(amount);
//                    reimbursements.add(manId);
//                    reimbursements.add(requDay);
//                    reimbursements.add(reason);

                }
            }
            return reimbursements;
        } catch (SQLException ex) {
            throw new ConnectionException("Error creating games list", ex);
        }
    }
}
