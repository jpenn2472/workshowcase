package com.ex.models;



public class Reimbursements {

    private double reimId;
    private double empId;
    private String status;
    private double amount;
    private double manId;
    private String requDay;
    private String reason;

    public Reimbursements(){
        super();
        this.reimId = 0;
        this.empId = 0;
        this.status = null;
        this.amount = 0;
        this.manId = 0;
        this.requDay = null;
        this.reason = null;
    }

    public Reimbursements(double reimId, double empId, String status, double amount, double manId, String requDay, String reason) {
        super();
        this.reimId = reimId;
        this.empId = empId;
        this.status = status;
        this.amount = amount;
        this.manId = manId;
        this.requDay = requDay;
        this.reason = reason;
    }

    public double getReimId() {
        return reimId;
    }

    public void setReimId(double reimId) {
        this.reimId = reimId;
    }

    public double getEmpId() {
        return empId;
    }

    public void setEmpId(double empId) {
        this.empId = empId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getManId() {
        return manId;
    }

    public void setManId(double manId) {
        this.manId = manId;
    }

    public String getRequDay() {
        return requDay;
    }

    public void setRequDay(String requDay) {
        this.requDay = requDay;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
    @Override
    public String toString() {
        return reimId + " " + empId + " " + status + " " + amount + " " + manId + " " + requDay + " " + reason;
    }


}





