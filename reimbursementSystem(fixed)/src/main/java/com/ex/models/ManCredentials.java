package com.ex.models;



public class ManCredentials {
    private double manId;
    private String fname;
    private String lname;
    private String empEmail;


    public double getManId() {
        return manId;
    }

    public void setManId(double manId) {
        this.manId = manId;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getEmpEmail() {
        return empEmail;
    }

    public void setEmpEmail(String empEmail) {
        this.empEmail = empEmail;
    }

    public ManCredentials(double empID, String fname, String lname, String empEmail) {
        this.manId = empID;
        this.fname = fname;
        this.lname = lname;
        this.empEmail = empEmail;


    }
}

