package com.ex.models;



public class Employee {
    private double empid;
    private String fname;
    private String lname;
    private String email;

    public Employee() {
        super();
        this.empid = 0;
        this.fname = null;
        this.lname = null;
        this.email = null;
    }


    public Employee(double empid, String fname, String lname, String email) {
        super();
        this.empid = empid;
        this.fname = fname;
        this.lname = lname;
        this.email = email;
    }

    public double getEmpid() {
        return empid;
    }

    public void setEmpid(double empid) {
        this.empid = empid;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getemail() {
        return email;
    }

    public void setemail(String email) {
        this.email = email;
    }

    public String toString() {
        return empid + " " + fname + " " + lname + " " + email + " ";
    }
}

