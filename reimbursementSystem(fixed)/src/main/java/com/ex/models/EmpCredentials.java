package com.ex.models;

public class EmpCredentials {
    private double empId;
    private String username;
    private String password;

    public EmpCredentials(double empId, String username, String password) {
        this.empId = empId;
        this.username = username;
        this.password = password;

    }

    public double getEmpId() {
        return empId;
    }

    public void setEmpId(double empId) {
        this.empId = empId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}


