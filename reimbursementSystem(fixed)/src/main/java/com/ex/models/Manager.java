package com.ex.models;



public class Manager {
    private double manid;
    private String fname;
    private String lname;
    private String manEmail;
    private String deptName;

    public Manager() {
        super();
        this.manid = 0;
        this.fname = null;
        this.lname = null;
        this.manEmail = null;
        this.deptName = null;
    }

    public Manager(double manid, String fname, String lname, String manEmail, String deptName) {
        this.manid = manid;
        this.fname = fname;
        this.lname = lname;
        this.manEmail = manEmail;
        this.deptName = deptName;
    }

    public double getManid() {
        return manid;
    }

    public void setManid(double manid) {
        this.manid = manid;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getManEmail() {
        return manEmail;
    }

    public void setManEmail(String manEmail) {
        this.manEmail = manEmail;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }
}