# workShowcase


**reimbursementSystem **

    We were asked to create a reimbursement system that had (at least) these requirements:
1. employee needed to request and view any reimbursements they have.
2. Manager needed to login and either approve or deny the requests
3. Admin could add employee and manager accounts
4. Had to use React, Typescript, sql, and java (no spring)
5. Had to be a single page application
6. had to use Aws to persist data between the application
7. Solo project

**ProjectTwo:Fitness Management Application**

This project would ask a new user a couple of fitness questions and would generate her a workout routine and diet depending on his/her preferences. For this project we had to come up with our own idea for an application that used an api, its requirements:

1. Needed to use an api to retrieve data
2. Had to be an inventive idea
3. Work with teams
4. Must use react, redux, java, css, html, postgres, api, and spring. (not spring boot)

This one did turn out to be a doozey cause we ran into a few major problems while working with it. 


Didn't have many requirements except use a new technology we haven't used before. We chose h2 database
Had to work in a group

**Last Repo: FreeCodeCampLessons.org**
    This is mainly what I've been working on during quarantine outside of family obligations. The website offers over thousands of hours of tutorials for free and I have been spending a lot more time with frontend technologies. I went ahead and uploaded all of the tutorials I have completed if you wanted to take a look.

Thank you so much for reading and if you have any questions or concerns email me at jpennington2472@gmail.com


